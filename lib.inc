%define SPACE 0x20
%define DECIMAL 0x30
%define NEWLINE 0xA
%define TAB 0x9
%define MINUS 0x2D
%define PLUS 0x2B
%define EOL 0
%define SYSEXIT 60
%define SYSREAD 0
%define SYSPRINT 1
%define STDIN 0
%define STDOUT 1
%define ERROR 0
%define VALID 1

section .text

; Accepts the return code and terminates the current process
exit: 
    mov rax, SYSEXIT            ;stop
    syscall

; Takes a pointer to a null-terminated string, returns its length
string_length:
    xor rax, rax
    .loop: 
        cmp byte[rdi + rax], EOL; end.of.line check
        je .end                 ; exit clause
        inc rax                 ; length increment
        jmp .loop
    .end:
        ret                     ; return str.ln

; Takes a pointer to a null-terminated string, outputs it to stdout
print_string:
    call string_length          ; str.ln to rax
    mov rsi, rdi                ; reference of start.of.string to rsi
    mov rdx, rax                ; str.ln(rax) to rdx
    mov rax, SYSPRINT           ; write
    mov rdi, STDOUT             ; stdout
    syscall
    ret

; Takes the character code and outputs it to stdout
print_char:
    push rdi                    ; code to stack
    mov rax, SYSPRINT           ; write
    mov rdi, STDOUT             ; stdout
    mov rdx, 1                  ; str.ln(1) to rdx
    mov rsi, rsp                ; reference of character in stack
    syscall
    pop rdi
    ret

; Translates a string (outputs a character with the code 0xA)
print_newline:
    mov rdi, NEWLINE            ; newline char `\n`
    jmp print_char              ; call printchar

; Outputs an unsigned 8-byte number in decimal format
; Tip: allocate a place in the stack and store the division results there
; Do not forget to translate the numbers into their ASCII codes.
print_uint:
    mov rax, rdi                ; number.to.divide to rax
    mov r9, rsp                 ; counter duties
    mov r8, 10                  ; needed for decimal div
    push 0                      ; end.of.number

    .loop:
        dec rsp
        mov rdx, 0              ; reset rdx
        div r8                  ; divide by 10
        add rdx, DECIMAL        ; convert to ascii
        mov byte[rsp], dl       ; number to stack
        cmp rax, 0              ; exit clause (if went over)
        je .end
        jmp .loop

    .end:
        mov rdi, rsp            ; address of number start
        push r9
        call print_string
        pop r9
        mov rsp, r9             ; shifting the stack
    ret

; Outputs a signed 8-byte number in decimal format
print_int:
    cmp rdi, 0                  ; compare with 0
    jge .printN                 ; >=0 clause
    push rdi
    mov rdi, MINUS
    call print_char             ; print '-'
    pop rdi
    neg rdi                     ; flip number, print as unsigned

    .printN:
        jmp print_uint 

; Takes two pointers to null-terminated strings, returns 1 if they are equal, 0 otherwise
string_equals:
    mov r8, rdi
    mov r9, rsi                 ; temp storage for both strings
    push rsi
    call string_length
    pop rdi
    push rax
    call string_length
    pop rdi                     ; length of 1 line
    cmp rax, rdi                ; compare str.ln
    jnz .not_equals

.loop:
    test rdi, rdi
    jz .equals
    mov r10b, [r8]
    mov r11b, [r9]
    cmp r10b, r11b
    jnz .not_equals
    inc r8                      ; current.char.1st.str +1
    inc r9                      ; current.char.2nd.str +1
    dec rdi
    jmp .loop

.not_equals:
    mov rax, ERROR
    ret

.equals:
    mov rax, VALID
    ret

; Reads one character from stdin and returns it. Returns 0 if the end of the stream is reached
read_char:
    push EOL                    ; error handling for empty str
    mov rax, SYSREAD            ; read
    mov rdi, STDIN              ; in
    mov rdx, 1              
    mov rsi, rsp                ; passing the buffer address
    syscall
    pop rax                     ; char to accumulator
    ret 

; Accepts: buffer start address, buffer size
; Reads a word from stdin into the buffer, skipping whitespace characters at the beginning.
; Whitespace characters are space 0x20, tab 0x9 and line feed 0xA.
; Stops and returns 0 if the word is too large for the buffer
; If successful, returns the buffer address in rax, the word length in rdx.
; Returns 0 in rax if it fails
; This function should append a null terminator to the word
read_word:
    mov rdx, 0                  ; counter

    .stripStart:
        push rdi                ; adress buffer
        push rsi                ; buffer size
        push rdx
        call read_char          ; we read the character of the string
        pop rdx
        pop rsi
        pop rdi

        cmp rax, SPACE          ; option space.char
        jz .stripStart
        cmp rax, TAB            ; option tab.char
        jz .stripStart
        cmp rax, NEWLINE        ; option newline.char
        jz .stripStart
        cmp rax, EOL            ; option end.of.line
        jz .error

    .loop:
        cmp rax, EOL            ; option end.of.line
        jz .end
        cmp rax, SPACE          ; option space.char
        jz .end
        cmp rax, TAB            ; option tab.char
        jz .end
        cmp rax, NEWLINE        ; option newline.char
        jz .end
        cmp rsi, rdx            ; if counter=str.ln, then error (word did not end properly)
        je .error
        mov byte[rdi+rdx], al   ; save changes
        inc rdx                 ; counter +1
        push rdi
        push rsi
        push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        jmp .loop

    .error:
        mov rax, ERROR
        mov rdx, ERROR
        ret

    .end:
        mov byte[rdi+rdx], EOL
        mov rax, rdi
        ret


; Takes a pointer to a string.
; Tries to read an unsigned number from its beginning.
; Returns in rax: a number, rdx : its length in characters
; rdx = 0 if the number could not be read by
parse_uint:
    xor rax, rax
    mov r9, 0                   ; number.ln
    mov r10, 10                 ; for decimal
    .loop:
        mov r11, 0              ; temp for low-order bits in rdi
        mov r11b, byte[rdi + r9]; lower bits of rdi + r9
        cmp r11, DECIMAL        ; check for 0
        jl .end                 ; under 0
        cmp r11, '9'            ; check for 9
        jg .end                 ; over 9 error
        cmp r11, EOL            ; check for end.of.line
        je .end
        sub r11b, DECIMAL       ; subtract 48 (from ascii to a number)
        mul r10                 ; decimal conversion
        add rax, r11            ; new digit
        inc r9                  ; digit counter
        jmp .loop
    
    .end:
        cmp r9, 0               ; digit counter check
        je .error               ; non-legal number
        mov rdx, r9             ; success, return number.ln
        ret

    .error:
        mov rax, ERROR          ; rax=0
        mov rdx, ERROR          ; rdx=0
        ret

; Takes a pointer to a string,
; Tries to read a signed number from its beginning.
; If there is a sign, spaces between it and the number are not allowed.
; Returns to rax: a number, rdx : its length in characters (including the sign, if there was one)
; rdx = 0 if the number could not be read by
parse_int:
    xor rdx, rdx
    cmp byte[rdi], PLUS         ; check for sign(+/-) char, proceed with signed logic
    je .signed
    cmp byte[rdi], MINUS
    je .signed
    jmp parse_uint              ; unsigned number
    jmp .end

    .signed:
        inc rdi                 ; skip sign char
        call parse_uint         ; read number
        cmp rdx, EOL            ; sign with no number
        je .end
        inc rdx                 ; increment num.ln
        cmp byte[rdi - 1], MINUS; if first char negative
        neg rax                 ; flip number
    
    .end:    
        ret

; Takes a pointer to a string, a pointer to a buffer and the length of the buffer
; Copies a string to the buffer
; Returns the length of the string if it fits in the buffer, otherwise 0
string_copy:
    ; rdi-pointer2str, rsi-pointer2buffer, rdx-buffer.ln
    push rdi                    ;rdi,rsi caller saved
    push rsi
    call string_length          ; rax=str.ln
    pop rsi
    pop rdi
    cmp rdx, rax
    jl .error
    mov rcx, 0                  ; char counter
    
    .loop:
        cmp rcx, rax            ; check str.ln vs buffer
        jg .end
        mov r8b, [rdi+rcx]      ; current char to accumulator
        mov byte[rsi+rcx], r8b  ; accumulator to buffer
        inc rcx                 ; char counter increment
        jmp .loop

    .error:
        mov rax, ERROR
        ret

    .end:
        ret                     ; rax=str.ln